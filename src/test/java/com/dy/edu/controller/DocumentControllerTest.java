package com.dy.edu.controller;


import com.dy.edu.AbstractControllerTest;
import com.dy.edu.integentity.DocumentIntegration;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.dy.edu.integservice.DocumentIntegrationService;

@Transactional
public class DocumentControllerTest extends AbstractControllerTest {

    @Mock
    private DocumentIntegrationService documentIntegrationService;
    
    @InjectMocks
    private DocumentController documentController;
    
    @Before
    public void setUp() {
        // Initialize Mockito annotated components
        MockitoAnnotations.initMocks(this);
        // Prepare the Spring MVC Mock components for standalone testing
        setUp(documentController);
    }
    @Test
    public void testGetGreeting() throws Exception {

        // Create some test data
        String id = "1";
        DocumentIntegration entity = getEntityStubData();
        List<DocumentIntegration> documents = new ArrayList<>();
        // Stub the GreetingService.findOne method return value
        when(documentIntegrationService.findByCreateID(id)).thenReturn(documents);

        // Perform the behavior being tested
        String uri = "/dy/download?createID="+id;

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri, id)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        // Extract the response status and body
        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();

        // Verify the GreetingService.findOne method was invoked once
        //verify(documentIntegrationService, times(1)).findOne(id);

        // Perform standard JUnit assertions on the test results
        Assert.assertEquals("failure - expected HTTP status 200", 200, status);
        Assert.assertTrue(
                "failure - expected HTTP response body to have a value",
                content.trim().length() > 0);
    }
	
    private Collection<DocumentIntegration> getEntityListStubData() {
        Collection<DocumentIntegration> list = new ArrayList<DocumentIntegration>();
        list.add(getEntityStubData());
        return list;
    }

    private DocumentIntegration getEntityStubData() {
    	DocumentIntegration entity = new DocumentIntegration();
        entity.setCreateID("1");
        entity.setCreateTS("12/23/2017");
        entity.setDocName("12.jpg");
        entity.setDocSize(123);
        entity.setDocUrl("http:www.google.com");
        entity.setFileType("Images");
        return entity;
    }
}
