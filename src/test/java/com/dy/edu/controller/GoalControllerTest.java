package com.dy.edu.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.dy.edu.AbstractControllerTest;
import com.dy.edu.GoalAbstractControllerTest;
import com.dy.edu.busentity.Goal;
import com.dy.edu.busservice.GoalService;
import com.dy.edu.integentity.DocumentIntegration;
import com.dy.edu.integentity.GoalIntegration;
import com.dy.edu.integservice.DocumentIntegrationService;
import com.dy.edu.integservice.GoalIntegrationService;

public class GoalControllerTest extends GoalAbstractControllerTest{

	   	@Mock
	   	private GoalService goalService;
	   	
	   	@Mock
	   	private GoalIntegrationService goalIntegrationService;
	    
	    @InjectMocks
	    private GoalController goalController;
	    
	    @Before
	    public void setUp() {
	        // Initialize Mockito annotated components
	        MockitoAnnotations.initMocks(this);
	        // Prepare the Spring MVC Mock components for standalone testing
	        setUp(goalController);
	    }
	    
/*	    @Test
	    public void testGetGoal() throws Exception {

	        // Create some test data
	        String id = "1";
	        Goal entity = getEntityStubData();
	        List<Goal> goal = new ArrayList<>();
	        // Stub the GreetingService.findOne method return value
	        when(goalService.findByCreateID(id)).thenReturn(goal);

	        // Perform the behavior being tested
	        String uri = "/dy/goal-data?createID="+id;

	        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri, id)
	                .accept(MediaType.APPLICATION_JSON)).andReturn();

	        // Extract the response status and body
	        String content = result.getResponse().getContentAsString();
	        int status = result.getResponse().getStatus();

	        // Verify the GreetingService.findOne method was invoked once
	        //verify(documentIntegrationService, times(1)).findOne(id);

	        // Perform standard JUnit assertions on the test results
	        Assert.assertEquals("failure - expected HTTP status 200", 200, status);
	        Assert.assertTrue(
	                "failure - expected HTTP response body to have a value",
	                content.trim().length() > 0);
	    }
*/	    @Test
	    public void testCreateGoal() throws Exception {

	        // Create some test data
	    	//Goal goal = getEntityStubData();

	    	GoalIntegration goaldto = getEntityStubData();
	        // Stub the GreetingService.create method return value
	        when(goalIntegrationService.save(any(GoalIntegration.class))).thenReturn(goaldto);

	        // Perform the behavior being tested
	        String uri = "/dy/save-goal";
	        String inputJson = super.mapToJson(goaldto);

	        MvcResult result = mvc
	                .perform(MockMvcRequestBuilders.post(uri)
	                        .contentType(MediaType.APPLICATION_JSON)
	                        .accept(MediaType.APPLICATION_JSON).content(inputJson))
	                .andReturn();

	        // Extract the response status and body
	        String content = result.getResponse().getContentAsString();
	        int status = result.getResponse().getStatus();

	        // Verify the GreetingService.create method was invoked once
	        //verify(goalIntegrationService, times(1)).save(any(GoalIntegration.class));

	        // Perform standard JUnit assertions on the test results
	        Assert.assertEquals("failure - expected HTTP status 200", 200, status);
	        Assert.assertTrue(
	                "failure - expected HTTP response body to have a value",
	                content.trim().length() > 0);

	        GoalIntegration createdEntity = super.mapFromJson(content, GoalIntegration.class);

	        Assert.assertNotNull("failure - expected entity not null",
	                createdEntity);
	        Assert.assertNotNull("failure - expected id attribute not null",
	                createdEntity.getId());
	        Assert.assertEquals("failure - expected text attribute match",
	        		goaldto.getGname(), createdEntity.getId());
	    }

	    private Collection<GoalIntegration> getEntityListStubData() {
	        Collection<GoalIntegration> list = new ArrayList<GoalIntegration>();
	        list.add(getEntityStubData());
	        return list;
	    }

	    private GoalIntegration getEntityStubData() {
	    	GoalIntegration entity = new GoalIntegration();
	        entity.setGname("xyz");
	        entity.setCreateID("1");
	        entity.setCreateTS(new Date());
	        entity.setGoalCategory("xyz");
	        entity.setGoalSubCategory("xyz");
	        entity.setGoalDesc("xyz");
	        return entity;
	    }
	}
