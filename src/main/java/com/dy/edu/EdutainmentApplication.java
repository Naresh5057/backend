package com.dy.edu;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
//@ComponentScan({"com.dy.edu.controller","com.dy.edu.busentity","com.dy.edu.busint","com.dy.edu.busrep","com.dy.edu.busservice","com.dy.edu.integentity","com.dy.edu.integint","com.dy.edu.integservice"})
public class EdutainmentApplication extends SpringBootServletInitializer{
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	    return application.sources(EdutainmentApplication.class);
	}
		
	public static void main(String[] args) {
		SpringApplication.run(EdutainmentApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
	@RequestMapping(value = "/fail")
	public String str(){
		return "fail";
	}

}