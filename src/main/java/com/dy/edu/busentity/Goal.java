package com.dy.edu.busentity;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import java.io.Serializable;
//import java.util.Date;
import java.util.Date;
//import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "goal")
@Where(clause = "ROW_STATUS_CD='I' or ROW_STATUS_CD='U'")
public class Goal implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private String gname;
	private String goalCategory;
	private String goalSubCategory;
	private String goalDesc;
	private String rowStatusCode;
	private String createID;
	private Date createTS;
	private String updateID;
	private Date updateTS;

	private Set<Rewards> rewards;
	private Initiatives initiatives;
	private Mentor mentor;

	public Goal() {
	}

	public Goal(String gname, String goalCategory, String goalSubCategory, String goalDesc, String rowStatusCode,
			String createID, Date createTS) {
		this.gname = gname;
		this.goalCategory = goalCategory;
		this.goalSubCategory = goalSubCategory;
		this.goalDesc = goalDesc;
		this.rowStatusCode = rowStatusCode;
		this.createID = createID;
		this.createTS = createTS;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "GL_ID")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "GL_NAME")
	public String getGname() {
		return gname;
	}

	public void setGname(String gname) {
		this.gname = gname;
	}

	@Column(name = "GL_CATG")
	public String getGoalCategory() {
		return goalCategory;
	}

	public void setGoalCategory(String goalCategory) {
		this.goalCategory = goalCategory;
	}

	@Column(name = "GL_SUB_CAT")
	public String getGoalSubCategory() {
		return goalSubCategory;
	}

	public void setGoalSubCategory(String goalSubCategory) {
		this.goalSubCategory = goalSubCategory;
	}

	@Column(name = "GL_DESC")
	public String getGoalDesc() {
		return goalDesc;
	}

	public void setGoalDesc(String goalDesc) {
		this.goalDesc = goalDesc;
	}

	@Column(name = "ROW_STATUS_CD")
	public String getRowStatusCode() {
		return rowStatusCode;
	}

	public void setRowStatusCode(String rowStatusCode) {
		this.rowStatusCode = rowStatusCode;
	}

	@Column(name = "CREATE_ID")
	public String getCreateID() {
		return createID;
	}

	public void setCreateID(String createID) {
		this.createID = createID;
	}

	@Column(name = "CREATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreateTS() {
		return createTS;
	}

	public void setCreateTS(Date createTS) {
		this.createTS = createTS;
	}

	@Column(name = "UPDATE_ID")
	public String getUpdateID() {
		return updateID;
	}

	public void setUpdateID(String updateID) {
		this.updateID = updateID;
	}

	@Column(name = "UPDATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "goal")
	public Set<Rewards> getRewards() {
		return rewards;
	}

	public void setRewards(Set<Rewards> rewards) {
		this.rewards = rewards;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "goals")
	public Initiatives getInitiatives() {
		return initiatives;
	}

	public void setInitiatives(Initiatives initiatives) {
		this.initiatives = initiatives;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "goal_men")
	public Mentor getMentor() {
		return mentor;
	}

	public void setMentor(Mentor mentor) {
		this.mentor = mentor;
	}

}