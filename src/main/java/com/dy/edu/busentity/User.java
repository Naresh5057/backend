package com.dy.edu.busentity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "user")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "USR_ID")
	private String userID;
	@Column(name = "PWD")
	private String password;
	@Column(name = "FIRST_NAM")
	private String firstName;
	@Column(name = "LAST_NAM")
	private String lastName;
	@Column(name = "ROW_STATUS_CD")
	private String rowStatusCode;
	@Column(name = "CREATE_ID")
	private String createID;
	@Column(name = "CREATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTS;
	@Column(name = "UPDATE_ID")
	private String updateID;
	@Column(name = "UPDATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTS;

	public User() {
	}

	public User(String userID, String password, String firstName, String lastName, String rowStatusCode,
			String createID, Date createTS) {
		this.userID = userID;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.rowStatusCode = rowStatusCode;
		this.createID = createID;
		this.createTS = createTS;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getRowStatusCode() {
		return rowStatusCode;
	}

	public void setRowStatusCode(String rowStatusCode) {
		this.rowStatusCode = rowStatusCode;
	}

	public String getCreateID() {
		return createID;
	}

	public void setCreateID(String createID) {
		this.createID = createID;
	}

	public Date getCreateTS() {
		return createTS;
	}

	public void setCreateTS(Date createTS) {
		this.createTS = createTS;
	}

	public String getUpdateID() {
		return updateID;
	}

	public void setUpdateID(String updateID) {
		this.updateID = updateID;
	}

	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}

	@Override
	public String toString() {
		return "User [userID=" + userID + ", password=" + password + ", firstName=" + firstName + ", lastName="
				+ lastName + ", rowStatusCode=" + rowStatusCode + ", createID=" + createID + ", createTS=" + createTS
				+ ", updateID=" + updateID + ", updateTS=" + updateTS + "]";
	}

}