package com.dy.edu.busentity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "login")
public class Logins {

	@Column(name = "U_ID")
	private int U_ID;
	@Id
	@Column(name = "USR_ID")
	private String userID;
	@Column(name = "PWD")
	private String password;
	@Column(name = "FIRST_NM")
	private String firstName;
	@Column(name = "LAST_NM")
	private String lastName;
	@Column(name = "ROLE_NM")
	private String roleName;
	@Column(name = "ROLE_CD")
	private String roleCD;
	@Column(name = "ADDR")
	private String address;
	@Column(name = "PHNO")
	private String phoneNumber;
	@Column(name = "ROW_STATUS_CD")
	private String rowStatusCode;
	@Column(name = "CREATE_ID")
	private String createID;
	@Column(name = "CREATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTS;
	@Column(name = "UPDATE_ID")
	private String updateID;
	@Column(name = "UPDATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTS;

	public Logins() {
	}

	public Logins(String userID, String password, String firstName, String lastName, String roleName, String roleCD,
			String address, String phoneNumber, String rowStatusCode, String createID, Date createTS) {
		super();
		this.userID = userID;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.roleName = roleName;
		this.roleCD = roleCD;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.rowStatusCode = rowStatusCode;
		this.createID = createID;
		this.createTS = createTS;
	}
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "GL_ID")
	public int getU_ID() {
		return U_ID;
	}

	public void setU_ID(int u_ID) {
		U_ID = u_ID;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleCD() {
		return roleCD;
	}

	public void setRoleCD(String roleCD) {
		this.roleCD = roleCD;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRowStatusCode() {
		return rowStatusCode;
	}

	public void setRowStatusCode(String rowStatusCode) {
		this.rowStatusCode = rowStatusCode;
	}

	public String getCreateID() {
		return createID;
	}

	public void setCreateID(String createID) {
		this.createID = createID;
	}

	public Date getCreateTS() {
		return createTS;
	}

	public void setCreateTS(Date createTS) {
		this.createTS = createTS;
	}

	public String getUpdateID() {
		return updateID;
	}

	public void setUpdateID(String updateID) {
		this.updateID = updateID;
	}

	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}

}