package com.dy.edu.busentity;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "initiatives")
@Where(clause = "ROW_STATUS_CD='I' or ROW_STATUS_CD='U'")
public class Initiatives implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer initID;
	private String initAction;
	private String startDate;
	private String targetDate;
	private String rowStatusCode;
	private String createID;
	private Date createTS;
	private String updateID;
	private Date updateTS;
	private String status;
	private String sponsor;
	private String initName;

	private Goal goals;

	public Initiatives() {
	}

	public Initiatives(String initAction, String startDate, String targetDate, String rowStatusCode, String createID,
			Date createTS, String status, Goal goals, String sponsor, String initName) {
		this.initAction = initAction;
		this.startDate = startDate;
		this.targetDate = targetDate;
		this.rowStatusCode = rowStatusCode;
		this.createID = createID;
		this.createTS = createTS;
		this.status = status;
		this.goals = goals;
		this.sponsor = sponsor;
		this.initAction = initAction;
		this.initName = initName;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "INIT_ID")
	public Integer getInitID() {
		return initID;
	}

	public void setInitID(Integer initID) {
		this.initID = initID;
	}

	@Column(name = "INIT_ACTION")
	public String getInitAction() {
		return initAction;
	}

	public void setInitAction(String initAction) {
		this.initAction = initAction;
	}

	@Column(name = "START_DATE")
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	@Column(name = "TARGET_DATE")
	public String getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}

	@Column(name = "ROW_STATUS_CD")
	public String getRowStatusCode() {
		return rowStatusCode;
	}

	public void setRowStatusCode(String rowStatusCode) {
		this.rowStatusCode = rowStatusCode;
	}

	@Column(name = "CREATE_ID")
	public String getCreateID() {
		return createID;
	}

	public void setCreateID(String createID) {
		this.createID = createID;
	}

	@Column(name = "CREATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreateTS() {
		return createTS;
	}

	public void setCreateTS(Date createTS) {
		this.createTS = createTS;
	}

	@Column(name = "UPDATE_ID")
	public String getUpdateID() {
		return updateID;
	}

	public void setUpdateID(String updateID) {
		this.updateID = updateID;
	}

	@Column(name = "INIT_NAME")
	public String getInitName() {
		return initName;
	}

	public void setInitName(String initName) {
		this.initName = initName;
	}

	@Column(name = "UPDATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}

	@Column(name = "STATUS")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@OneToOne
	@JoinColumn(name = "GL_ID")
	@JsonIgnore
	public Goal getGoals() {
		return goals;
	}

	public void setGoals(Goal goals) {
		this.goals = goals;
	}

	@Column(name = "GL_SPON")
	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

}