package com.dy.edu.busentity;

import java.util.Date;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "document")
@Where(clause = "ROW_STATUS_CD='I' or ROW_STATUS_CD='U'")
public class Document implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DOC_ID")
	private int docID;
	@Column(name = "DOC_NM")
	private String docName;
	@Column(name = "DOC_SZ")
	private long docSize;
	@Column(name = "FIL_TYP")
	private String fileType;
	@Column(name = "DOC_URL")
	private String docUrl;
	@Column(name = "ST_CD")
	private String statusCode;
	@Column(name = "ROW_STATUS_CD")
	private String rowStatusCode;
	@Column(name = "CREATE_ID")
	private String createID;
	@Column(name = "CREATE_TS")
	private String createTS;
	@Column(name = "UPDATE_ID")
	private String updateID;
	@Column(name = "UPDATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTS;

	public Document() {
	}

	public Document(String docName, long docSize, String fileType, String docUrl, String statusCode,
			String rowStatusCode, String createID, String createTS) {
		super();
		this.docName = docName;
		this.docSize = docSize;
		this.fileType = fileType;
		this.statusCode = statusCode;
		this.rowStatusCode = rowStatusCode;
		this.createID = createID;
		this.createTS = createTS;
		this.docUrl = docUrl;
	}

	public int getDocID() {
		return docID;
	}

	public void setDocID(int docID) {
		this.docID = docID;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public long getDocSize() {
		return docSize;
	}

	public void setDocSize(long docSize) {
		this.docSize = docSize;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getDocUrl() {
		return docUrl;
	}

	public void setDocUrl(String docUrl) {
		this.docUrl = docUrl;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getRowStatusCode() {
		return rowStatusCode;
	}

	public void setRowStatusCode(String rowStatusCode) {
		this.rowStatusCode = rowStatusCode;
	}

	public String getCreateID() {
		return createID;
	}

	public void setCreateID(String createID) {
		this.createID = createID;
	}

	public String getCreateTS() {
		return createTS;
	}

	public void setCreateTS(String createTS) {
		this.createTS = createTS;
	}

	public String getUpdateID() {
		return updateID;
	}

	public void setUpdateID(String updateID) {
		this.updateID = updateID;
	}

	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}

}