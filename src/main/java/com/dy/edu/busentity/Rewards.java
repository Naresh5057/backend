package com.dy.edu.busentity;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "rewards")
@Where(clause = "ROW_STATUS_CD='I' or ROW_STATUS_CD='U'")
public class Rewards implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer rewardID;
	private String rewardName;
	private String rowStatusCode;
	private String createID;
	private Date createTS;
	private String updateID;
	private Date updateTS;

	private Goal goal;

	public Rewards() {
	}

	public Rewards(String rewardName) {

		this.rewardName = rewardName;
	}

	public Rewards(Goal goal, String rewardName, String rowStatusCode, String createID, Date createTS) {
		this.goal = goal;
		this.rewardName = rewardName;
		this.rowStatusCode = rowStatusCode;
		this.createID = createID;
		this.createTS = createTS;

	}

	public Rewards(Goal goalID, String rewardName) {

		this.goal = goalID;
		this.rewardName = rewardName;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "RWD_ID")
	public Integer getRewardID() {
		return rewardID;
	}

	public void setRewardID(Integer rewardID) {
		this.rewardID = rewardID;
	}

	@Column(name = "RWD_NAME")
	public String getRewardName() {
		return rewardName;
	}

	public void setRewardName(String rewardName) {
		this.rewardName = rewardName;
	}

	@Column(name = "ROW_STATUS_CD")
	public String getRowStatusCode() {
		return rowStatusCode;
	}

	public void setRowStatusCode(String rowStatusCode) {
		this.rowStatusCode = rowStatusCode;
	}

	@Column(name = "CREATE_ID")
	public String getCreateID() {
		return createID;
	}

	public void setCreateID(String createID) {
		this.createID = createID;
	}

	@Column(name = "CREATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreateTS() {
		return createTS;
	}

	public void setCreateTS(Date createTS) {
		this.createTS = createTS;
	}

	@Column(name = "UPDATE_ID")
	public String getUpdateID() {
		return updateID;
	}

	public void setUpdateID(String updateID) {
		this.updateID = updateID;
	}

	@Column(name = "UPDATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}

	@ManyToOne
	@JoinColumn(name = "GL_ID")
	@JsonIgnore
	public Goal getGoal() {
		return goal;
	}

	public void setGoal(Goal goal) {
		this.goal = goal;
	}

}