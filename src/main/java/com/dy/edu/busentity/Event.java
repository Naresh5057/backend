package com.dy.edu.busentity;

import java.util.Date;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "event")
public class Event implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "EVENT_ID")
	private int eventID;
	@Column(name = "EVENT_NM")
	private String eventName;
	@Column(name = "START_AT")
	private String startAT;
	@Column(name = "END_AT")
	private String endAT;
	@Column(name = "ROW_STATUS_CD")
	private String rowStatusCode;
	@Column(name = "CREATE_ID")
	private String createID;
	@Column(name = "CREATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTS;
	@Column(name = "UPDATE_ID")
	private String updateID;
	@Column(name = "UPDATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTS;

	public Event() {
	}

	public Event(String eventName, String startAT, String endAT, String rowStatusCode, String createID, Date createTS) {
		super();

		this.eventName = eventName;
		this.startAT = startAT;
		this.endAT = endAT;
		this.rowStatusCode = rowStatusCode;
		this.createID = createID;
		this.createTS = createTS;
	}

	public int getEventID() {
		return eventID;
	}

	public void setEventID(int eventID) {
		this.eventID = eventID;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getStartAT() {
		return startAT;
	}

	public void setStartAT(String startAT) {
		this.startAT = startAT;
	}

	public String getEndAT() {
		return endAT;
	}

	public void setEndAT(String endAT) {
		this.endAT = endAT;
	}

	public String getRowStatusCode() {
		return rowStatusCode;
	}

	public void setRowStatusCode(String rowStatusCode) {
		this.rowStatusCode = rowStatusCode;
	}

	public String getCreateID() {
		return createID;
	}

	public void setCreateID(String createID) {
		this.createID = createID;
	}

	public Date getCreateTS() {
		return createTS;
	}

	public void setCreateTS(Date createTS) {
		this.createTS = createTS;
	}

	public String getUpdateID() {
		return updateID;
	}

	public void setUpdateID(String updateID) {
		this.updateID = updateID;
	}

	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}

}