package com.dy.edu.busentity;

import java.io.Serializable;

public class Resources implements Serializable {

	private static final long serialVersionUID = 1L;

	private String rurl;

	public String getRurl() {
		return rurl;
	}

	public void setRurl(String rurl) {
		this.rurl = rurl;
	}

	public Resources(String rurl) {
		this.rurl = rurl;
	}

	public Resources() {
	}

	@Override
	public String toString() {
		return "Resources [rurl=" + rurl + "]";
	}

}