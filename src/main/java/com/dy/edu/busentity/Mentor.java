package com.dy.edu.busentity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "mentor")
public class Mentor implements Serializable {

	private static final long serialVersionUID = 1L;

	private int mentorID;
	private String mentorName;
	private String mentorPosition;
	private String mentorArea;
	private String rowStatusCode;
	private String createID;
	private Date createTS;
	private String updateID;
	private Date updateTS;

	private byte[] img;

	private Goal goal_men;

	public Mentor() {
	}

	public Mentor(String mentorName, String mentorPosition, String mentorArea, String rowStatusCode, String createID,
			Date createTS, Goal goal_men, byte[] img) {

		this.mentorName = mentorName;
		this.mentorPosition = mentorPosition;
		this.mentorArea = mentorArea;
		this.rowStatusCode = rowStatusCode;
		this.createID = createID;
		this.createTS = createTS;
		this.goal_men = goal_men;
		this.img = img;

	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "MENT_ID")
	public int getMentorID() {
		return mentorID;
	}

	public void setMentorID(int mentorID) {
		this.mentorID = mentorID;
	}

	@Column(name = "MENT_NAME")
	public String getMentorName() {
		return mentorName;
	}

	public void setMentorName(String mentorName) {
		this.mentorName = mentorName;
	}

	@Column(name = "MENT_POSITION")
	public String getMentorPosition() {
		return mentorPosition;
	}

	public void setMentorPosition(String mentorPosition) {
		this.mentorPosition = mentorPosition;
	}

	@Column(name = "MENT_AREA")
	public String getMentorArea() {
		return mentorArea;
	}

	public void setMentorArea(String mentorArea) {
		this.mentorArea = mentorArea;
	}

	@Column(name = "ROW_STATUS_CD")
	public String getRowStatusCode() {
		return rowStatusCode;
	}

	public void setRowStatusCode(String rowStatusCode) {
		this.rowStatusCode = rowStatusCode;
	}

	@Column(name = "CREATE_ID")
	public String getCreateID() {
		return createID;
	}

	public void setCreateID(String createID) {
		this.createID = createID;
	}

	@Column(name = "CREATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreateTS() {
		return createTS;
	}

	public void setCreateTS(Date createTS) {
		this.createTS = createTS;
	}

	@Column(name = "UPDATE_ID")
	public String getUpdateID() {
		return updateID;
	}

	public void setUpdateID(String updateID) {
		this.updateID = updateID;
	}

	@Column(name = "UPDATE_TS")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}

	@Column(name = "PIC")
	public byte[] getImg() {
		return img;
	}

	public void setImg(byte[] img) {
		this.img = img;
	}

	@OneToOne
	@JoinColumn(name = "GL_ID")
	@JsonIgnore
	public Goal getGoal_men() {
		return goal_men;
	}

	public void setGoal_men(Goal goal_men) {
		this.goal_men = goal_men;
	}

}