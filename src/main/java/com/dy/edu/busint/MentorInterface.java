package com.dy.edu.busint;

import java.util.List;

import com.dy.edu.busentity.Mentor;

public interface MentorInterface {

	public void save(Mentor mentor);

	public void update(Mentor mentor);

	public void delete(Mentor mentor);

	public List<Mentor> findAll();

	public String findOne(int id);

}