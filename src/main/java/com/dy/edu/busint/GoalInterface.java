package com.dy.edu.busint;

import java.util.List;

import com.dy.edu.busentity.Goal;

public interface GoalInterface {

	public void save(Goal goal);

	public void update(Goal goal);

	public void delete(Goal goal);

	public List<Goal> findByCreateID(String createID);

	public List<Goal> findByGoalID(int id);

	public List<Goal> findAll();

}