package com.dy.edu.busint;

import com.dy.edu.busentity.Initiatives;

public interface InitiativesInterface {

	public void save(Initiatives initatives);

	public void update(Initiatives initiatives);

	public void delete(Initiatives initiatives);

}