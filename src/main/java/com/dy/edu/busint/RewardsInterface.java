package com.dy.edu.busint;

import com.dy.edu.busentity.Rewards;

public interface RewardsInterface {

	public void save(Rewards rewards);

	public void update(Rewards rewards);

}