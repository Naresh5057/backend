package com.dy.edu.busint;

import java.util.List;

import com.dy.edu.busentity.Document;

public interface DocumentInterface {

	public void save(Document document);

	public void delete(Document document);

	public List<Document> findAll();

	public String findOne(int id);

	public List<Document> findByCreateID(String createID);

	public Document findByDocID(int id);

}