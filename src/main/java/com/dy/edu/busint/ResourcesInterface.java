package com.dy.edu.busint;

import java.io.IOException;
import java.util.List;

import com.dy.edu.busentity.Resources;

public interface ResourcesInterface {

	public List<Resources> viewallSearch(String name) throws IOException;

}