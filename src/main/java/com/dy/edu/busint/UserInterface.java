package com.dy.edu.busint;

import com.dy.edu.busentity.User;

public interface UserInterface {

	public String findAll(String usr, String pwd);

	public void save(User user);
	
}