package com.dy.edu.busservice;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.dy.edu.busint.UploadBusinessInterface;
import com.dy.edu.controller.Credentials;

public class UploadBusinessService implements UploadBusinessInterface {

	BasicAWSCredentials awsCreds = new BasicAWSCredentials(Credentials.access_key_id, Credentials.secret_access_key);
	@SuppressWarnings("deprecation")
	AmazonS3Client s3Client = new AmazonS3Client(awsCreds);
	String bucket = "dyfolder";

	// File upload s3 sample
	public void uploadfileS3_i(byte[] bytes, String fileName, String fileType) {

		String UdtfileName = "1" + fileName;

		String folderName = "com.dy.edu.doc/" + fileType;

		InputStream stream1 = new ByteArrayInputStream(bytes);
		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentLength(bytes.length);
		PutObjectResult d = s3Client
				.putObject(new PutObjectRequest(bucket, folderName + "/" + UdtfileName, stream1, meta)
						.withCannedAcl(CannedAccessControlList.PublicRead));

		System.out.println("business" + d);
	}
	// End File upload s3 sample
}