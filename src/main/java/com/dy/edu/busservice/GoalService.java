package com.dy.edu.busservice;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.dy.edu.busentity.Goal;
import com.dy.edu.busint.GoalInterface;
import com.dy.edu.busrep.GoalRepository;

@Service
@Transactional
public class GoalService implements GoalInterface {

	private GoalRepository goalRepository;

	public GoalService(GoalRepository goalRepository) {
		this.goalRepository = goalRepository;
	}

	public void save(Goal goal) {
		goalRepository.save(goal);
	}

	public void update(Goal goal) {
		Goal updateGoal = goalRepository.findOne(goal.getId());
		updateGoal.setGname(goal.getGname());
		updateGoal.setGoalCategory(goal.getGoalCategory());
		updateGoal.setGoalSubCategory(goal.getGoalSubCategory());
		updateGoal.setGoalDesc(goal.getGoalDesc());
		updateGoal.setRowStatusCode("U");
		updateGoal.setUpdateID(goal.getUpdateID());
		updateGoal.setUpdateTS(goal.getUpdateTS());
	}

	public void delete(Goal goal) {
		Goal deleteGoal = goalRepository.findOne(goal.getId());
		deleteGoal.setRowStatusCode("D");
	}

	public List<Goal> findAll() {
		List<Goal> goals = new ArrayList<>();

		for (Goal goal : goalRepository.findAll()) {
			goals.add(goal);
		}
		System.out.println("result " + goals);
		return goals;
	}

	public List<Goal> findByCreateID(String createID) {
		List<Goal> goals = new ArrayList<>();

		for (Goal goal : goalRepository.findByCreateID(createID)) {
			goals.add(goal);
		}
		return goals;
	}

	public List<Goal> findByGoalID(int id) {
		List<Goal> goals = new ArrayList<>();

		for (Goal goal : goalRepository.findById(id)) {
			goals.add(goal);
		}

		return goals;
	}

}