package com.dy.edu.busservice;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.dy.edu.busentity.Mentor;
import com.dy.edu.busint.MentorInterface;
import com.dy.edu.busrep.MentorRepository;
import com.dy.edu.controller.Credentials;

@Service
@Transactional
public class MentorService implements MentorInterface {

	private final MentorRepository mentorRepository;

	public MentorService(MentorRepository mentorRepository) {
		this.mentorRepository = mentorRepository;
	}
	
	
	
	BasicAWSCredentials awsCreds = new BasicAWSCredentials(Credentials.access_key_id, Credentials.secret_access_key);
	@SuppressWarnings("deprecation")
	AmazonS3Client s3Client = new AmazonS3Client(awsCreds);
	String bucket = "dyfolder";
	
	

	public void save(Mentor mentor) {
		mentorRepository.save(mentor);
	}

	public void update(Mentor mentor) {
		Mentor updateMentor = mentorRepository.findOne(mentor.getMentorID());
		updateMentor.setRowStatusCode("U");
		updateMentor.setMentorName(mentor.getMentorName());
		updateMentor.setMentorArea(mentor.getMentorArea());
		updateMentor.setMentorPosition(mentor.getMentorPosition());
		updateMentor.setCreateTS(mentor.getCreateTS());
	}

	public void delete(Mentor mentor) {
		Mentor deleteMentor = mentorRepository.findOne(mentor.getMentorID());
		deleteMentor.setRowStatusCode("D");
	}

	public String findOne(int id) {
		String mentor = mentorRepository.findOne(id).toString();
		return mentor;
	}

	public List<Mentor> findAll() {
		List<Mentor> mentors = new ArrayList<>();

		for (Mentor mentor : mentorRepository.findAll()) {
			mentors.add(mentor);
		}

		return mentors;
	}

}