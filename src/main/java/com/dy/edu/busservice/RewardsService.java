package com.dy.edu.busservice;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.dy.edu.busentity.Rewards;
import com.dy.edu.busint.RewardsInterface;
import com.dy.edu.busrep.RewardsRepository;

@Service
@Transactional
public class RewardsService implements RewardsInterface {

	private final RewardsRepository rewardrepository;

	public RewardsService(RewardsRepository rewardrepository) {
		this.rewardrepository = rewardrepository;
	}

	public void save(Rewards rewards) {
		rewardrepository.save(rewards);
	}

	public void update(Rewards rewards) {
		Rewards updateRewards = rewardrepository.findOne(rewards.getRewardID());
		updateRewards.setRowStatusCode("U");
		updateRewards.setRewardName(rewards.getRewardName());
		updateRewards.setUpdateID(rewards.getUpdateID());
		updateRewards.setUpdateTS(rewards.getUpdateTS());
	}

}