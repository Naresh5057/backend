package com.dy.edu.busservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.springframework.stereotype.Service;

import com.dy.edu.busentity.Resources;
import com.dy.edu.busint.ResourcesInterface;

@Service
@Transactional
public class ResourcesService implements ResourcesInterface {

	public static final String GOOGLE_SEARCH_URL = "https://www.google.com/search";

	public List<Resources> viewallSearch(String name) throws IOException {

		int num = 10;
		List<String> gog = new ArrayList<String>();
		String searchURL = GOOGLE_SEARCH_URL + "?q=" + name + "&num=" + num;
		Document doc = Jsoup.connect(searchURL).userAgent("Mozilla/5.0").get();
		Elements results = doc.select("h3.r > a");
		List<Resources> entities = new ArrayList<Resources>();

		for (Element result : results) {
			String linkHref = result.attr("href");
			String url = linkHref.substring(6, linkHref.indexOf("&"));
			String Orgurl = url.substring(1);
			Resources res = new Resources();
			
			
			String str2 = "https";
			if(Orgurl.toString().toLowerCase().contains(str2.toLowerCase())){
				res.setRurl(Orgurl);
			}
			else{
				continue;
			}
			entities.add(res);
		}

		return entities;

	}

}