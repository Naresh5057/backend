package com.dy.edu.busservice;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;

import com.dy.edu.busentity.Document;
import com.dy.edu.busrep.DocumentRepository;
import com.dy.edu.controller.Credentials;

@Service
@Transactional
public class DocumentService {

	private final DocumentRepository documentRepository;

	public DocumentService(DocumentRepository documentRepository) {
		this.documentRepository = documentRepository;
	}

	/*This Code represents the save operation for the document*/
	public void save(Document document) {
		document.setRowStatusCode("I");
		documentRepository.save(document);
	}

	/*This code represents the delete operation for the document*/
	public void delete(Document document) {
		Document deleteDocument = documentRepository.findOne(document.getDocID());
		deleteDocument.setRowStatusCode("D");

	}

	/*This code represents for retrieving all the datas from the document table*/
	public List<Document> findAll() {
		List<Document> documents = new ArrayList<>();
		for (Document document : documentRepository.findAll()) {
			documents.add(document);
		}
		return documents;
	}

	public String findOne(int id) {
		String document = documentRepository.findOne(id).toString();
		return document;
	}

	public List<Document> findByCreateID(String createID) {
		List<Document> documents = new ArrayList<>();
		for (Document document : documentRepository.findByCreateID(createID)) {
			documents.add(document);
		}
		return documents;
	}

	public Document findByDocID(int id) {
		List<Document> documents = new ArrayList<>();
		for (Document document : documentRepository.findByDocID(id)) {
			documents.add(document);
		}
		return this.documentRepository.findOne(id);
	}

	BasicAWSCredentials awsCreds = new BasicAWSCredentials(Credentials.access_key_id, Credentials.secret_access_key);
	@SuppressWarnings("deprecation")
	AmazonS3Client s3Client = new AmazonS3Client(awsCreds);
	String bucket = "dyfolder";

	// CREATING METADATA FOR UPLOADING FILE
	public void createfileMetaData_i(String fileName, String fileType) {
		System.out.println("hellocontroller " + fileName);
	}

	// File upload s3 sample
	public void uploadfileS3_i(byte[] bytes, String fileName, String fileType) {

		String UdtfileName = "1" + fileName;

		String folderName = "com.dy.edu.doc/" + fileType;

		InputStream stream1 = new ByteArrayInputStream(bytes);
		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentLength(bytes.length);
		PutObjectResult d = s3Client
				.putObject(new PutObjectRequest(bucket, folderName + "/" + UdtfileName, stream1, meta)
						.withCannedAcl(CannedAccessControlList.PublicRead));

		System.out.println("business" + d);
	}
	// End File upload s3 sample
}