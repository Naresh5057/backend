package com.dy.edu.busservice;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.dy.edu.busentity.Initiatives;
import com.dy.edu.busint.InitiativesInterface;
import com.dy.edu.busrep.InitiativesRepository;

@Service
@Transactional
public class InitiativesService implements InitiativesInterface {

	private final InitiativesRepository initativesRepository;

	public InitiativesService(InitiativesRepository initativesRepository) {
		this.initativesRepository = initativesRepository;
	}

	public void save(Initiatives initatives) {
		initativesRepository.save(initatives);
	}

	public void update(Initiatives initiatives) {
		Initiatives updateInitiatives = initativesRepository.findOne(initiatives.getInitID());
		updateInitiatives.setRowStatusCode("U");
		updateInitiatives.setInitAction(initiatives.getInitAction());
		updateInitiatives.setStatus(initiatives.getStatus());
		updateInitiatives.setUpdateID(initiatives.getUpdateID());
		updateInitiatives.setUpdateTS(initiatives.getUpdateTS());
	}
	

	public void delete(Initiatives initiatives) {
		Initiatives deleteInitiatives = initativesRepository.findOne(initiatives.getInitID());
		deleteInitiatives.setRowStatusCode("D");
	}

}