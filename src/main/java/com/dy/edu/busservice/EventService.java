package com.dy.edu.busservice;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.dy.edu.busentity.Document;
import com.dy.edu.busentity.Event;
import com.dy.edu.busrep.EventRepository;

@Service
@Transactional
public class EventService {

	private final EventRepository eventRepository;

	public EventService(EventRepository eventRepository) {
		this.eventRepository = eventRepository;
	}

	public void save(Event goal) {
		eventRepository.save(goal);
	}

	public List<Event> findAll() {
		List<Event> events = new ArrayList<>();

		for (Event event : eventRepository.findAll()) {
			events.add(event);
		}

		return events;
	}

	public List<Event> findByCreateID(String createID) {
		List<Event> events = new ArrayList<>();
		for (Event event : eventRepository.findByCreateID(createID)) {
			events.add(event);
		}
		return events;
	}
	public void delete(int id) {
		Event deleteGl = eventRepository.findOne(id);
		deleteGl.setRowStatusCode("D");
	}

}