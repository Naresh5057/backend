package com.dy.edu.busservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.dy.edu.busentity.EmailAddress;

@Service
public class EmailAddressService {

	private JavaMailSender javaMailSender;

	@Autowired
	public EmailAddressService(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	public void sendNotification(EmailAddress emailAddress) throws MailException {
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(emailAddress.getEmailAddress());
		mail.setFrom("datayaandemo@gmail.com");
		mail.setSubject("Toys");
		mail.setText("http://localhost:4200/confirmpwd?email=" + emailAddress.getEmailAddress());

		javaMailSender.send(mail);
	}

}