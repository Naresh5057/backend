package com.dy.edu.busservice;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.dy.edu.busentity.Logins;

import com.dy.edu.busrep.LoginRepository;

@Service
@Transactional
public class LoginService {

	private final LoginRepository userRepository;

	public LoginService(LoginRepository userRepository) {

		this.userRepository = userRepository;
	}

	
	
/*	public String findAll(String usr, String pwd) {
		//List<User> users = new ArrayList<>();
		String s=null;

		for (Logins user : userRepository.findAll()) {
		
			if(user.getUserID().equals(usr) && user.getPassword().equals(pwd))
			{
				s = "true";
			break;
			}
			else
			{
				s="false";
				
			}
			}
		
		
		return s;
	}*/
	
	
	public List<Logins> findAll(String usr, String pwd) {
		List<Logins> goals = new ArrayList<>();

		for (Logins goal : userRepository.findAll()) {
			
			if(goal.getUserID().equals(usr) && goal.getPassword().equals(pwd))
			goals.add(goal);
		}
		//System.out.println("result " + goals);
		return goals;
	}
	

	public void save(Logins user) {
		userRepository.save(user);
	}

	/*public void delete(String id) {
		userRepository.delete(id);
	}

	public String findOne(String id) {
		String result = userRepository.findOne(id).toString();
		return result;
	}

	public void update(String id, String name) {
		User updatedDoc = userRepository.findOne(id);
		updatedDoc.setUserID(id);
		updatedDoc.setFirstName(name);
	}*/

	/*public List<User> findByCreateID(String createID) {
		List<User> users = new ArrayList<>();

		for (User user : userRepository.findByCreateID(createID)) {
			users.add(user);
		}

		return users;
	}*/
	
	/*public String findAll(String usr, String pwd) {
	List<User> users = new ArrayList<>();
	String s=null;

	for (User user : userRepository.findAll()) {
	
		users.add(user);
	}
	for(User us: users){
	if(us.getUserID().equals(usr) && us.getPassword().equals(pwd))
		s = "true";
	else
		s="false";
	}
	return s;
}*/
	
	
	public void update(String name, String password) {
		Logins updatedDoc = userRepository.findOne(name);
		updatedDoc.setPassword(password);
	
	}
	
}