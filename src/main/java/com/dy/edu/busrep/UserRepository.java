package com.dy.edu.busrep;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.dy.edu.busentity.User;

@Transactional
public interface UserRepository extends CrudRepository<User, String> {

}