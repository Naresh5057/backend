package com.dy.edu.busrep;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.dy.edu.busentity.Event;

@Transactional
public interface EventRepository extends CrudRepository<Event, Integer> {

	public List<Event> findByCreateID(String create);
}