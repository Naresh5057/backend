package com.dy.edu.busrep;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.dy.edu.busentity.Mentor;

@Transactional
public interface MentorRepository extends CrudRepository<Mentor, Integer> {

}