package com.dy.edu.busrep;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.dy.edu.busentity.Initiatives;

@Transactional
public interface InitiativesRepository extends CrudRepository<Initiatives, Integer> {

}