package com.dy.edu.busrep;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dy.edu.busentity.Goal;

public interface GoalRepository extends JpaRepository<Goal, Integer> {

	public List<Goal> findByCreateID(String create);

	public List<Goal> findById(int id);
}