package com.dy.edu.busrep;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.dy.edu.busentity.Logins;

@Transactional
public interface LoginRepository extends CrudRepository<Logins, String> {

}