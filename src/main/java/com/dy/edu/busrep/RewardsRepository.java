package com.dy.edu.busrep;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.dy.edu.busentity.Rewards;

@Transactional
public interface RewardsRepository extends CrudRepository<Rewards, Integer> {

}