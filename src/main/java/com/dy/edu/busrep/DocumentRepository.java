package com.dy.edu.busrep;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.dy.edu.busentity.Document;

@Transactional
public interface DocumentRepository extends CrudRepository<Document, Integer> {

	public List<Document> findByCreateID(String create);

	public List<Document> findByDocID(int id);

}