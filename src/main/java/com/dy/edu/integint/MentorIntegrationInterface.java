package com.dy.edu.integint;

import com.dy.edu.integentity.MentorIntegration;

public interface MentorIntegrationInterface {

	public void save(MentorIntegration mentorIntegration);
	
	public void convertToEntity(MentorIntegration mentorIntegration);
	
	public void update(MentorIntegration mentorIntegration);
	
	public void delete(MentorIntegration mentorIntegration);
}