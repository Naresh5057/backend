package com.dy.edu.integint;

import com.dy.edu.integentity.RewardsIntegration;

public interface RewardsIntegrationInterface {

	public void save(RewardsIntegration rewardsIntegration);

	public void convertToEntity(RewardsIntegration rewardsIntegration);

	public void update(RewardsIntegration rewardsIntegration);
	
}