package com.dy.edu.integint;

import com.dy.edu.integentity.InitiativesIntegration;

public interface InitiativesIntegrationInterface {

	public void save(InitiativesIntegration initiatives);
	
	public void convertToEntity(InitiativesIntegration initiatives);
	
	public void update(InitiativesIntegration initiatives);
	
	public void delete(InitiativesIntegration  initiatives);
	
}