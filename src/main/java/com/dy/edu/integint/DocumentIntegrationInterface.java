package com.dy.edu.integint;

import java.util.List;

import com.dy.edu.busentity.Document;
import com.dy.edu.integentity.DocumentIntegration;

public interface DocumentIntegrationInterface {

	public List<DocumentIntegration> findByCreateID(String createID);

	public List<DocumentIntegration> convertToDto(List<Document> list);

	public void uploadfileS3(byte[] bytes, String fileName, String fileType);
	
	public Document convertToEntity(DocumentIntegration postDto);

}