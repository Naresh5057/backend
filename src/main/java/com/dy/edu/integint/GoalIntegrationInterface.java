package com.dy.edu.integint;

import java.util.List;

import com.dy.edu.busentity.Goal;
import com.dy.edu.integentity.GoalIntegration;

public interface GoalIntegrationInterface {

	public List<GoalIntegration> findByCreateID(String createID);
	
	public List<GoalIntegration> convertToDto(List<Goal> list);
	
	public GoalIntegration save(GoalIntegration goal);
	
	public void convertToEntity(GoalIntegration goalDto);
	
	public void update(GoalIntegration goalDto);
	
	public void delete(GoalIntegration goalDto);
	
}