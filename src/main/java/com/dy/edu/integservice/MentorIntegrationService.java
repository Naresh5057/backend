/*package com.dy.edu.integservice;

import java.util.Date;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dy.edu.busentity.Mentor;
import com.dy.edu.busservice.MentorService;
import com.dy.edu.integentity.MentorIntegration;
import com.dy.edu.integint.MentorIntegrationInterface;

@Service
@Transactional
public class MentorIntegrationService implements MentorIntegrationInterface {

	@Autowired
	public MentorService mentorService;

	@Autowired
	public ModelMapper modelMapper;

	public void save(MentorIntegration mentorIntegration) {

		convertToEntity(mentorIntegration);

	}

	public void convertToEntity(MentorIntegration mentorIntegration) {

		Mentor mentor = modelMapper.map(mentorIntegration, Mentor.class);

		mentor.setRowStatusCode("I");
		mentor.setCreateID("1");
		mentor.setCreateTS(new Date());

		mentorService.save(mentor);

	}

	public void update(MentorIntegration mentorIntegration) {

		Mentor mentor = modelMapper.map(mentorIntegration, Mentor.class);
		mentorService.update(mentor);

	}

	public void delete(MentorIntegration mentorIntegration) {

		Mentor mentor = modelMapper.map(mentorIntegration, Mentor.class);
		mentorService.delete(mentor);

	}

}*/
package com.dy.edu.integservice;
public class MentorIntegrationService{}