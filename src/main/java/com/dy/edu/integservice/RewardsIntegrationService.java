package com.dy.edu.integservice;

import java.util.Date;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dy.edu.busentity.Rewards;
import com.dy.edu.busservice.RewardsService;
import com.dy.edu.integentity.RewardsIntegration;
import com.dy.edu.integint.RewardsIntegrationInterface;

@Service
@Transactional
public class RewardsIntegrationService implements RewardsIntegrationInterface {

	@Autowired
	public RewardsService rewardsService;

	@Autowired
	public ModelMapper modelMapper;

	public void save(RewardsIntegration rewardsIntegration) {

		convertToEntity(rewardsIntegration);

	}

	public void convertToEntity(RewardsIntegration rewardsIntegration) {

		Rewards rewards = modelMapper.map(rewardsIntegration, Rewards.class);

		rewards.setRowStatusCode("I");
		rewards.setCreateID("1");
		rewards.setCreateTS(new Date());

		rewardsService.save(rewards);

	}

	public void update(RewardsIntegration rewardsIntegration) {

		Rewards rewards = modelMapper.map(rewardsIntegration, Rewards.class);
		rewards.setUpdateID("1");
		rewards.setUpdateTS(new Date());
		rewardsService.update(rewards);

	}

}