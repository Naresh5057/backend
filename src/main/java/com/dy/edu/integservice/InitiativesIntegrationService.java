package com.dy.edu.integservice;

import java.util.Date;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dy.edu.busentity.Initiatives;
import com.dy.edu.busservice.InitiativesService;
import com.dy.edu.integentity.InitiativesIntegration;
import com.dy.edu.integint.InitiativesIntegrationInterface;

@Service
@Transactional
public class InitiativesIntegrationService implements InitiativesIntegrationInterface {

	@Autowired
	public InitiativesService initiativesService;

	@Autowired
	public ModelMapper modelMapper;

	public void save(InitiativesIntegration initiativesIntegration) {

		convertToEntity(initiativesIntegration);

	}

	public void convertToEntity(InitiativesIntegration initiativesIntegration) {

		Initiatives initiatives = modelMapper.map(initiativesIntegration, Initiatives.class);

		
		initiatives.setInitAction("Start");
		initiatives.setStatus("grey");
		initiatives.setRowStatusCode("I");
		initiatives.setCreateID("1");
		initiatives.setCreateTS(new Date());

		initiativesService.save(initiatives);

	}

	public void update(InitiativesIntegration initiativesIntegration) {

		Initiatives initiatives = modelMapper.map(initiativesIntegration, Initiatives.class);
		initiatives.setInitID(initiativesIntegration.getInitID());
		initiatives.setUpdateID("1");
		initiatives.setUpdateTS(new Date());
		initiativesService.update(initiatives);

	}

	public void delete(InitiativesIntegration initiativesIntegration) {

		Initiatives initiatives = modelMapper.map(initiativesIntegration, Initiatives.class);
		initiativesService.delete(initiatives);

	}

}