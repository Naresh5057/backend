package com.dy.edu.integservice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;

import com.dy.edu.busentity.Document;
import com.dy.edu.busservice.DocumentService;
import com.dy.edu.busservice.UploadBusinessService;
import com.dy.edu.controller.Credentials;
import com.dy.edu.integentity.DocumentIntegration;
import com.dy.edu.integint.DocumentIntegrationInterface;

@Service
@Transactional
public class DocumentIntegrationService implements DocumentIntegrationInterface {

	@Autowired
	public DocumentService documentService;

	@Autowired
	public ModelMapper modelMapper;

	UploadBusinessService uis = new UploadBusinessService();

	BasicAWSCredentials awsCreds = new BasicAWSCredentials(Credentials.access_key_id, Credentials.secret_access_key);

	@SuppressWarnings("deprecation")
	AmazonS3Client s3Client = new AmazonS3Client(awsCreds);

	String bucket = "dyfolder";

	public void uploadfileS3(byte[] bytes, String fileName, String fileType) {
		uis.uploadfileS3_i(bytes, fileName, fileType);
	}

	public List<DocumentIntegration> findByCreateID(String createID) {

		return convertToDto(documentService.findByCreateID(createID));

	}

	public List<DocumentIntegration> convertToDto(List<Document> list) {

		List<DocumentIntegration> documents = new ArrayList<>();

		for (Document document : list) {
			DocumentIntegration postDto = modelMapper.map(document, DocumentIntegration.class);
			documents.add(postDto);
		}

		return documents;
	}

	public Document convertToEntity(DocumentIntegration documentIntegration) {

		Document document = modelMapper.map(documentIntegration, Document.class);

		document.setStatusCode("Success");
		document.setCreateTS(new Date().toString());

		documentService.save(document);

		return document;
	}
	
	public void delete(DocumentIntegration documentIntegration) {
		Document document = modelMapper.map(documentIntegration, Document.class);
		documentService.delete(document);

	}

}