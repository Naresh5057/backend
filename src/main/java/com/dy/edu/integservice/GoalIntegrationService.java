package com.dy.edu.integservice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dy.edu.busentity.Goal;
import com.dy.edu.busservice.GoalService;
import com.dy.edu.integentity.GoalIntegration;
import com.dy.edu.integint.GoalIntegrationInterface;

@Service
@Transactional
public class GoalIntegrationService implements GoalIntegrationInterface {

	@Autowired
	public GoalService goalService;

	@Autowired
	public ModelMapper modelMapper;

	public List<GoalIntegration> findByCreateID(String createID) {
		return convertToDto(goalService.findByCreateID(createID));
	}

	public List<GoalIntegration> convertToDto(List<Goal> list) {

		List<GoalIntegration> documents1 = new ArrayList<>();

		for (Goal document : list) {
			GoalIntegration postDto1 = modelMapper.map(document, GoalIntegration.class);
			documents1.add(postDto1);
		}

		return documents1;
	}

	public GoalIntegration save(GoalIntegration goal) {
		convertToEntity(goal);
		GoalIntegration goaldto = null;
		return goaldto;
	}

	public void convertToEntity(GoalIntegration goalDto) {

		Goal goal = modelMapper.map(goalDto, Goal.class);

		goal.setRowStatusCode("I");
		goal.setCreateTS(new Date());

		goalService.save(goal);

	}

	public void update(GoalIntegration goalIntegration) {
		Goal goal = modelMapper.map(goalIntegration, Goal.class);
		goal.setUpdateID("1");
		goal.setUpdateTS(new Date());
		goalService.update(goal);

	}

	public void delete(GoalIntegration goalIntegration) {
		Goal goal = modelMapper.map(goalIntegration, Goal.class);
		goalService.delete(goal);

	}

}