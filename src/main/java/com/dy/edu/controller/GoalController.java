package com.dy.edu.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dy.edu.busentity.Event;
import com.dy.edu.busentity.Goal;
import com.dy.edu.busservice.GoalService;
import com.dy.edu.integentity.GoalIntegration;
import com.dy.edu.integservice.GoalIntegrationService;

@RestController
@RequestMapping(value = "/dy")
public class GoalController {

	private static Logger logger = LogManager.getLogger();
	@Autowired
	private GoalIntegrationService goalIntegrationService;

	@Autowired
	private GoalService goalService;

	@CrossOrigin
	@RequestMapping(value = "/save-goal", method = RequestMethod.POST)
    //public String saveGoal(@RequestParam("glname") String glName, @RequestParam("glCatg") String glCatg,
			//@RequestParam("glSubCat") String glSubCat, @RequestParam("glDesc") String glDesc,@RequestParam("createID") String createID) {
	public String saveGoal(@RequestBody GoalIntegration goaldto) {
		/*GoalIntegration goaldto = new GoalIntegration();
		goaldto.setGname(glName);
		goaldto.setGoalCategory(glCatg);
		goaldto.setGoalSubCategory(glSubCat);
		goaldto.setGoalDesc(glDesc);
		goaldto.setCreateID(createID);*/
		try{
			goalIntegrationService.save(goaldto);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return "success";
	}

	@CrossOrigin
	@RequestMapping(value = "/update-goal", method = RequestMethod.POST)
	public Filesuccess updateGoal(@RequestParam("glid") int glid, @RequestParam("glname") String glname,
			@RequestParam("Selectcategory") String Selectcategory,
			@RequestParam("Selectsubcategory") String Selectsubcategory, @RequestParam("gldesc") String gldesc) {
		GoalIntegration goaldto = new GoalIntegration();
		goaldto.setId(glid);
		goaldto.setGname(glname);
		goaldto.setGoalCategory(Selectcategory);
		goaldto.setGoalSubCategory(Selectsubcategory);
		goaldto.setGoalDesc(gldesc);
		try{
			goalIntegrationService.update(goaldto);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return new Filesuccess("update success");
	}

	@CrossOrigin
	@GetMapping("/delete-goal")
	public String deleteGoal(@RequestParam int id) {
		GoalIntegration goaldto = new GoalIntegration();
		goaldto.setId(id);
		try{
			goalIntegrationService.delete(goaldto);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "Task Deleted!";
	}

	@CrossOrigin
	@GetMapping("/viewall-goal")
	public List<Goal> viewallGoal() {
		
		List<Goal> goal = new ArrayList<>();
		try{
			goal =  goalService.findAll();
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return goal;
	}

	@CrossOrigin
	@RequestMapping("/success2")
	public List<GoalIntegration> viewgoal() {
		String createID = "1";

		return goalIntegrationService.findByCreateID(createID);
	}

	@CrossOrigin
	@RequestMapping("/get-goal")
	public List<Goal> viewoneGoal(@RequestParam int id) {
		List<Goal> goal = new ArrayList<>();
		try{
			goal = goalService.findByGoalID(id);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return goal;
	}

	@CrossOrigin
	@GetMapping("/goal-data")
	public List<Goal> goalData(@RequestParam("createID") String createID) {
		//String createID = "1";
		List<Goal> goal = new ArrayList<>();
		try{
			goal = goalService.findByCreateID(createID);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return goalService.findByCreateID(createID);
	}

	
}