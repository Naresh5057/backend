package com.dy.edu.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dy.edu.busentity.EmailAddress;
import com.dy.edu.busservice.EmailAddressService;

@RestController
@RequestMapping(value = "/dy")
public class EmailAddressController {

	private static Logger logger = LogManager.getLogger();
	@Autowired
	private EmailAddressService emailAddressService;

	@RequestMapping("/signup")
	public String signup() {
		return "Please sign up for our service";
	}

	@CrossOrigin
	@RequestMapping("/signup-success")
	public String signupSuccess(@RequestParam("email") String email) {
		System.out.println("Hello" + email);

		EmailAddress emailAddress = new EmailAddress();
		emailAddress.setEmailAddress(email);

		try {
			emailAddressService.sendNotification(emailAddress);
		} catch (MailException ex) {
			logger.error(ex.getMessage());
		}

		return "Thanks you for registering with us";
	}
}