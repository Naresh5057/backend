package com.dy.edu.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.dy.edu.busentity.Event;
import com.dy.edu.busentity.Goal;
import com.dy.edu.busentity.Initiatives;
import com.dy.edu.busservice.InitiativesService;
import com.dy.edu.integentity.GoalIntegration;
import com.dy.edu.integentity.InitiativesIntegration;
import com.dy.edu.integservice.InitiativesIntegrationService;

@SuppressWarnings("unused")
@RestController
@RequestMapping(value = "/dy")
public class InitiativesController {

	private static Logger logger = LogManager.getLogger();
	@Autowired
	private InitiativesService initiativesService;

	@Autowired
	private InitiativesIntegrationService initiativesIntegrationService;

	@CrossOrigin
	@RequestMapping(value = "/save-init", method = RequestMethod.POST)
	public String saveInitiatives(@RequestParam("glid") Goal goalID, @RequestParam("sponsor") String sponsor,
			@RequestParam("startDate") String startDate, @RequestParam("targetDate") String targetDate,
			@RequestParam("initname") String initname) {
		InitiativesIntegration initiativesIntegration = new InitiativesIntegration();
		
		initiativesIntegration.setGoals(goalID);
		initiativesIntegration.setSponsor(sponsor);
		initiativesIntegration.setInitName(initname);
		initiativesIntegration.setStartDate(startDate);
		initiativesIntegration.setTargetDate(targetDate);

		try{
			initiativesIntegrationService.save(initiativesIntegration);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return "Data Saved Successfully";
	}

	@CrossOrigin
	@RequestMapping("/update-init")
	public String updateInitiatives(@RequestParam("iid") Integer id, @RequestParam("action") String action,
			@RequestParam("status") String status) {
		InitiativesIntegration initiativesIntegration = new InitiativesIntegration();
		initiativesIntegration.setInitID(id);
		initiativesIntegration.setInitAction(action);
		initiativesIntegration.setStatus(status);
		
		try{
			initiativesIntegrationService.save(initiativesIntegration);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		initiativesIntegrationService.update(initiativesIntegration);
		return "Initiatives Updated!";
	}

	@CrossOrigin
	@GetMapping("/delete-init")
	public void deleteInitiatives(@RequestParam int initID) {
		InitiativesIntegration initiativesIntegration = new InitiativesIntegration();
		initiativesIntegration.setInitID(initID);
		try{
			initiativesIntegrationService.delete(initiativesIntegration);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		
	}

}