package com.dy.edu.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dy.edu.busentity.Event;
import com.dy.edu.busservice.EventService;
import com.dy.edu.integentity.DocumentIntegration;

@RestController
@RequestMapping(value = "/dy")
public class EventController {
	
	private static Logger logger = LogManager.getLogger();
	@Autowired
	private EventService eventService;

	@CrossOrigin
	@GetMapping("/viewall-event")
	public List<Event> viewallGoal() {
		return eventService.findAll();
	}
	
	@CrossOrigin
	@GetMapping("/view-event")
	public List<Event> viewevent(@RequestParam("createID") String createID) {
		List<Event> event = new ArrayList<>();
		try{
			event = eventService.findByCreateID(createID);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return event;
	}
	
	@CrossOrigin
	@RequestMapping(value ="/save-event")
	public String saveEvent(@RequestParam("eventName") String eventName,
			@RequestParam("startAT") String startAT, @RequestParam("endAT") String endAT) {
		Event event = new Event(eventName, startAT, endAT, "I", "1", new Date());
		try{
			eventService.save(event);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "Event Saved!";
	}
	
	@CrossOrigin
	@GetMapping("/delete-event")
	public String deleteEvent(@RequestParam int id) {

		try{
			eventService.delete(id);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "Success";
	}

}