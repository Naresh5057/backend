/*package com.dy.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dy.edu.busentity.Goal;
import com.dy.edu.busservice.MentorService;
import com.dy.edu.integentity.MentorIntegration;
import com.dy.edu.integservice.MentorIntegrationService;

@RestController
@RequestMapping(value = "/dy")
public class MentorController {

	@Autowired
	private MentorIntegrationService mentorIntegrationService;

	@Autowired
	private MentorService mentorService;
	
	@CrossOrigin
	@RequestMapping("/save-mentor")
	public String saveMentor(@RequestParam("mentname") String mentorName,
			@RequestParam("position") String mentorPosition, @RequestParam("area") String mentorArea,
			@RequestParam("goalname") Goal goalID) {

		MentorIntegration mentorIntegration = new MentorIntegration();
		mentorIntegration.setMentorName(mentorName);
		mentorIntegration.setMentorPosition(mentorPosition);
		mentorIntegration.setMentorArea(mentorArea);
		mentorIntegration.setGoal(goalID);
		mentorIntegrationService.save(mentorIntegration);
		
		
		return "Mentor Saved!";
	}

}*/


package com.dy.edu.controller;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dy.edu.busentity.*;

import com.dy.edu.busservice.MentorService;

@RestController
@RequestMapping(value = "/dy")
public class MentorController {

	private static Logger logger = LogManager.getLogger();
	@Autowired
	private MentorService mentorService;

	@CrossOrigin
	@RequestMapping("/save-mentor")
	public String saveMentor(@RequestParam("mentname") String mentorName,
			@RequestParam("position") String mentorPosition, @RequestParam("area") String mentorArea,
			@RequestParam("goalname") Goal goalID, byte[] img ) {
		Mentor mentors = new Mentor(mentorName, mentorPosition, mentorArea, "I", "1", new Date(), goalID,img);
		try{
			mentorService.save(mentors);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}	
		return "Mentor Saved!";
	}

}


