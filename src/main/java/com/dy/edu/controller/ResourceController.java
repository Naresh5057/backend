package com.dy.edu.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dy.edu.busentity.Event;
import com.dy.edu.busentity.Resources;
import com.dy.edu.busservice.ResourcesService;

@RestController
@RequestMapping(value = "/dy")
public class ResourceController {

	private static Logger logger = LogManager.getLogger();
	@Autowired
	private ResourcesService googleSer;

	@CrossOrigin
	@RequestMapping("/viewall-search")
	public List<Resources> viewallSearch(@RequestParam String sname) throws IOException {
		
		List<Resources> resources = new ArrayList<>();
		try{
			resources = googleSer.viewallSearch(sname);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return resources;
	}

}