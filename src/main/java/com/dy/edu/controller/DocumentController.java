package com.dy.edu.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.dy.edu.busentity.Document;
import com.dy.edu.busservice.DocumentService;
import com.dy.edu.integentity.DocumentIntegration;
import com.dy.edu.integservice.DocumentIntegrationService;

@SuppressWarnings("unused")
@RestController
@RequestMapping(value = "/dy")
public class DocumentController {

	private static final HttpServletResponse HttpServletResponse = null;
	
	@Autowired
	private DocumentService documentService;
	@Autowired
	private DocumentIntegrationService documentIntegrationService;

	String fileType;

	private static Logger logger = LogManager.getLogger();
	@RequestMapping(value = "/success")
	public String str(){
        logger.debug("This is a debug message");
        logger.info("This is an info message");
        logger.warn("This is a warn message");
        logger.error("This is an error message");
        logger.fatal("This is a fatal message");
		return "success";
	}
	@CrossOrigin
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public Filesuccess upload(@RequestParam("uploadFile") MultipartFile formdata,
			@RequestParam("fileType") String fileType,@RequestParam("createID") String createID) {
		String c_id = createID;
		String fname = c_id + formdata.getOriginalFilename();
		long fsize = formdata.getSize();
		System.out.println(fileType);
		this.fileType = fileType;
		String bucket = "dyfolder";
		String folder = "com.dy.edu.doc/" + fileType;
		String reqUrl = "https://s3.ap-south-1.amazonaws.com/" + bucket + "/" + folder + "/" + fname;

		DocumentIntegration documentIntegration = new DocumentIntegration();

		documentIntegration.setCreateID(createID);
		documentIntegration.setDocName(fname);
		documentIntegration.setFileType(fileType);
		documentIntegration.setDocSize(fsize);
		documentIntegration.setDocUrl(reqUrl);

		try {
			documentIntegrationService.convertToEntity(documentIntegration);
		}
		catch(Exception e){
			logger.error(e);
		}
		formdata.getSize();
		DocumentIntegrationService ups = new DocumentIntegrationService();

		byte[] bytes;
		try {
			bytes = formdata.getBytes();
			System.out.println(bytes);
			String fileName = formdata.getOriginalFilename();

			ups.uploadfileS3(bytes, fileName, fileType);
		} catch (IOException e) {

			logger.error(e.getMessage());
		}
		return new Filesuccess("file success");
	}

	@CrossOrigin
	@RequestMapping("/download")
	public List<DocumentIntegration> viewallDocument(@RequestParam("createID") String createID) {
		//String createID = "1";
		List<DocumentIntegration> doc = new ArrayList<>();
		try{
			doc = documentIntegrationService.findByCreateID(createID);
		}
		catch(Exception e){
			logger.error(e.getMessage());
		}
		return doc;
	}

	@CrossOrigin
	@GetMapping("/delete-task")
	public void deleteDocument(@RequestParam int id) {

		DocumentIntegration documentIntegration = new DocumentIntegration();
		documentIntegration.setDocID(id);
		documentIntegrationService.delete(documentIntegration);

	}

	@CrossOrigin
	@RequestMapping("/viewone-task")
	public Document viewoneDocument(@RequestParam int id) {
		return documentService.findByDocID(id);
	}

}