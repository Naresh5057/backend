package com.dy.edu.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dy.edu.busentity.User;
import com.dy.edu.busservice.UserService;

@RestController
@RequestMapping(value = "/dy")
public class UserController {

	@Autowired
	private UserService userService;

	@CrossOrigin
	@GetMapping("/viewall-users")
	public Login viewallUser(@RequestParam("username") String usrid, @RequestParam("password") String pwd) {
		if(userService.findAll(usrid, pwd).equals("true"))
			return new Login(true);
		
		else
			return new Login(false);
	}

	@CrossOrigin
	@GetMapping("/save-users")
	public String saveUser(@RequestParam String userID, @RequestParam String password, @RequestParam String firstName,
			@RequestParam String lastName, @RequestParam String createID) {
		User user = new User(userID, password, firstName, lastName, "I", createID, new Date());
		userService.save(user);
		return "User Saved!";
	}
	
}