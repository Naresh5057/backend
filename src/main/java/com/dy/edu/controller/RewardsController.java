package com.dy.edu.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dy.edu.busentity.Goal;
import com.dy.edu.integentity.RewardsIntegration;
import com.dy.edu.integservice.RewardsIntegrationService;

@RestController
@RequestMapping(value = "/dy")
public class RewardsController {

	private static Logger logger = LogManager.getLogger();
	@Autowired
	private RewardsIntegrationService rewardsIntegrationService;

	@CrossOrigin
	@RequestMapping("/save-rewards")
	public String saveRewards(@RequestParam("rname") String rewardName, @RequestParam("glid") Goal goalID) {
		
		RewardsIntegration rewardsIntegration = new RewardsIntegration();
		rewardsIntegration.setRewardName(rewardName);
		rewardsIntegration.setGoal(goalID);
		try{
			rewardsIntegrationService.save(rewardsIntegration);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}	
		return "Reward Saved!";
	}

	@CrossOrigin
	@RequestMapping(value = "/update-rewards", method = RequestMethod.POST)
	public String updateRewards(@RequestParam("reid") int rwdID, @RequestParam("rewname") String rewardName) {
		
		RewardsIntegration rewardsIntegration = new RewardsIntegration();
		rewardsIntegration.setRewardID(rwdID);
		rewardsIntegration.setRewardName(rewardName);
		try{
			rewardsIntegrationService.update(rewardsIntegration);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}	
		
		return "Reward Updated!";
	}

}