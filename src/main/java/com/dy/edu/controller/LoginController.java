package com.dy.edu.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dy.edu.busentity.Event;
import com.dy.edu.busentity.Logins;
import com.dy.edu.busservice.LoginService;

@RestController
@RequestMapping(value = "/dy")
public class LoginController {

	private static Logger logger = LogManager.getLogger();
	@Autowired
	private LoginService userService;

	/*
	 * @CrossOrigin
	 * 
	 * @GetMapping("/viewall-user") public Login
	 * viewallUser(@RequestParam("username") String
	 * username, @RequestParam("password") String password) {
	 * if(userService.findAll(username, password).equals("true")) return new
	 * Login(true);
	 * 
	 * else return new Login(false); }
	 */

	@CrossOrigin
	@GetMapping("/viewall-user")
	public List<Logins> viewallGoal(@RequestParam("username") String username,
			@RequestParam("password") String password) {
		List<Logins> login = new ArrayList<>();
		try{
			login = userService.findAll(username, password);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return login;
	}

	@CrossOrigin
	@GetMapping("/update-user")
	public String updateUser(@RequestParam("userID") String userID, @RequestParam("password") String password) {

		try{
			userService.update(userID, password);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}	
		return "success";

	}

	@CrossOrigin
	@RequestMapping(value = "/save-user", method = RequestMethod.POST)
	public String saveUser(@RequestParam("userID") String userID, @RequestParam("password") String password,
			@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName,
			@RequestParam("roleName") String roleName, @RequestParam("address") String address,
			@RequestParam("phoneNumber") String phoneNumber) {
		String roleCD = null;
		if (roleName.equals("Parent"))
			roleCD = "P";

		else if (roleName.equals("Mentor"))
			roleCD = "M";

		else
			roleCD = "S";
		
		Logins user = new Logins(userID, password, firstName, lastName, roleName, roleCD, address, phoneNumber, "I",
				"1", new Date());
		try{
			userService.save(user);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}	
		
		return "User Saved!";
	}

}