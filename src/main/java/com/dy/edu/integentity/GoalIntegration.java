package com.dy.edu.integentity;

import java.util.Date;
import java.util.Set;

public class GoalIntegration {

	private int id;
	private String gname;
	private String goalCategory;
	private String goalSubCategory;
	private String goalDesc;
	private String createID;
	private Date createTS;
	private Date updateTS;

	private Set<RewardsIntegration> rewards;
	private InitiativesIntegration initiatives;
	private MentorIntegration mentor;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGname() {
		return gname;
	}

	public void setGname(String gname) {
		this.gname = gname;
	}

	public String getGoalCategory() {
		return goalCategory;
	}

	public void setGoalCategory(String goalCategory) {
		this.goalCategory = goalCategory;
	}

	public String getGoalSubCategory() {
		return goalSubCategory;
	}

	public void setGoalSubCategory(String goalSubCategory) {
		this.goalSubCategory = goalSubCategory;
	}

	public String getGoalDesc() {
		return goalDesc;
	}

	public void setGoalDesc(String goalDesc) {
		this.goalDesc = goalDesc;
	}

	public Date getCreateTS() {
		return createTS;
	}

	public void setCreateTS(Date createTS) {
		this.createTS = createTS;
	}

	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}

	public InitiativesIntegration getInitiatives() {
		return initiatives;
	}

	public void setInitiatives(InitiativesIntegration initiatives) {
		this.initiatives = initiatives;
	}

	public Set<RewardsIntegration> getRewards() {
		return rewards;
	}

	public void setRewards(Set<RewardsIntegration> rewards) {
		this.rewards = rewards;
	}

	public MentorIntegration getMentor() {
		return mentor;
	}

	public void setMentor(MentorIntegration mentor) {
		this.mentor = mentor;
	}

	public String getCreateID() {
		return createID;
	}

	public void setCreateID(String createID) {
		this.createID = createID;
	}
	

}