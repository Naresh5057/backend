package com.dy.edu.integentity;

import java.util.Date;

import com.dy.edu.busentity.Goal;

public class InitiativesIntegration {

	private Integer initID;
	private String initAction;
	private String startDate;
	private String targetDate;
	private Date createTS;
	private Date updateTS;
	private String status;
	private String sponsor;
	private String initName;
	
	private Goal goals;

	public Integer getInitID() {
		return initID;
	}

	public void setInitID(Integer initID) {
		this.initID = initID;
	}

	public String getInitAction() {
		return initAction;
	}

	public void setInitAction(String initAction) {
		this.initAction = initAction;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}

	public Date getCreateTS() {
		return createTS;
	}

	public void setCreateTS(Date createTS) {
		this.createTS = createTS;
	}

	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getInitName() {
		return initName;
	}

	public void setInitName(String initName) {
		this.initName = initName;
	}

	public Goal getGoals() {
		return goals;
	}

	public void setGoals(Goal goals) {
		this.goals = goals;
	}

	

}