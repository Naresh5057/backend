package com.dy.edu.integentity;

import java.util.Date;

import com.dy.edu.busentity.Goal;

public class RewardsIntegration {

	private Integer rewardID;
	private String rewardName;
	private Date createTS;
	private Date updateTS;

	private Goal goal;
	
	public Integer getRewardID() {
		return rewardID;
	}

	public void setRewardID(Integer rewardID) {
		this.rewardID = rewardID;
	}

	public String getRewardName() {
		return rewardName;
	}

	public void setRewardName(String rewardName) {
		this.rewardName = rewardName;
	}

	public Date getCreateTS() {
		return createTS;
	}

	public void setCreateTS(Date createTS) {
		this.createTS = createTS;
	}

	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}

	public Goal getGoal() {
		return goal;
	}

	public void setGoal(Goal goal) {
		this.goal = goal;
	}

}